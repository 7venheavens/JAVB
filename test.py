import sys
from PyQt4 import QtGui
import os
import re
import pymongo



class Example(QtGui.QMainWindow):
	def __init__(self):
		super().__init__()

		self.initUI()
		self.loaded = dict()
		self.title_regex = re.compile(r"""(\w+)-?([0-9]+)""")

	def debug(self, text):
		self.textwidget.text_edit.append(text )

	def is_video(self, filename):
		if os.path.splitext(filename)[1] in {".avi", ".rmvb", ".mkv", ".mp4"}:
			return True
		return 0
	def update_table(self, table):
		table.clearContents()
		entries = sorted(self.loaded.keys)

	def get_folder(self):
		def get_code(dir):
			for i in os.listdir(dir):
				target = os.path.join(dir, i)
				temp = self.title_regex.search(target)
				# print(temp)
				if temp and self.is_video(target):
					#check if more than one file has the name
					key = "-".join(temp.groups())
					if key in self.loaded:
						self.loaded[key].append(os.path.abspath(i))
					else:
						self.loaded[key] = [os.path.abspath(i)]
					return
				else:
					if os.path.isdir(i):
						get_code(os.path.join(dir, i))

		fname = QtGui.QFileDialog.getExistingDirectory(self, "Load folder into JAVB", )
		self.statusBar().showMessage("Loading directory")
		for i in os.listdir(fname):
			# create relative path
			target = os.path.join(fname, i)

			#if title detected in folder/file name
			temp = self.title_regex.search(target)
			try:
				print("target: ",target)
			except UnicodeEncodeError:
				print("Unicode_error")
			#print("temp: ", temp.groups(0))
			self.textwidget.text_edit.append(str(target))
			self.textwidget.text_edit.append(str(self.is_video(target)))
			self.textwidget.text_edit.append(str(os.path.isdir(os.path.join(fname, target))))
			if temp and self.is_video(target):
				key = "-".join(temp.groups())
				if key in self.loaded:
					self.loaded[key].append(os.path.abspath(target))
				else:
					self.loaded[key] = [os.path.abspath(target)]
			else:
				if os.path.isdir(target):
					self.debug("deep search" + target )
					get_code(target)
		self.textwidget.text_edit.append(str(self.loaded))	
		self.textwidget.text_edit.append("DONE")
		print("1")
		self.statusBar().showMessage("Loading complete")




	def initUI(self):
		exitAction = QtGui.QAction(QtGui.QIcon("exit.png"), "&Exit", self)
		exitAction.setShortcut("Ctrl+Q")
		exitAction.setStatusTip("Exit Application")
		exitAction.triggered.connect(QtGui.qApp.quit)

		openFold = QtGui.QAction(QtGui.QIcon("open.png"), "&Load folder", self)
		openFold.setShortcut("Ctrl+O")
		openFold.setStatusTip("Load folder into JAVB")
		openFold.triggered.connect(self.get_folder)

		statusbar = self.statusBar()
		menubar = self.menuBar()
		fileMenu = menubar.addMenu("&File")
		fileMenu.addAction(exitAction)
		fileMenu.addAction(openFold)
		self.textwidget = TestWidget()
		self.setCentralWidget(self.textwidget)
		self.setGeometry(300, 300, 1024, 768)

		self.show()

class TestWidget(QtGui.QWidget):
	def __init__(self):
		super().__init__()
		grid = QtGui.QGridLayout()
		grid.setSpacing(10)
		

		self.text_edit = QtGui.QTextEdit()
		self.table_widget = QtGui.QTableWidget()
		self.table_widget.setRowCount(5)
		self.table_widget.setColumnCount(5)
		self.table_widget.setHorizontalHeaderItem(0, QtGui.QTableWidgetItem("Code"))
		self.table_widget.setHorizontalHeaderItem(1, QtGui.QTableWidgetItem("Title"))
		self.table_widget.setHorizontalHeaderItem(2, QtGui.QTableWidgetItem("Tags"))
		self.table_widget.setHorizontalHeaderItem(3, QtGui.QTableWidgetItem("column_3"))
		self.table_widget.setHorizontalHeaderItem(4, QtGui.QTableWidgetItem("column_4"))
		self.table_widget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)

		grid.addWidget(self.text_edit, 0, 0, 3,0)
		grid.addWidget(self.table_widget, 4, 0, 6, 0)

		self.setLayout(grid)



if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	ex = Example()
	sys.exit(app.exec_())