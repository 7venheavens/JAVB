import sys
import os
import subprocess
from pymongo import MongoClient
import re
from javsearch import JAVSearch
from bson.objectid import ObjectId

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import Qt


client = MongoClient()
db = client.JAVB
core = db.core

"""
to implement:
	put ids in tablewidget
	hide the id table
"""



class Main(QMainWindow):
	def __init__(self):
		super().__init__()

		self.initUI()
		self.loaded = dict()
		self.title_regex = re.compile(r"""([a-zA-Z]+)-?([0-9]+)""")
		self.searcher = JAVSearch()

	def save(self):
		pass

	def debug(self, text):
		self.debug_widget.append(text )

	def is_video(self, filename):
		if os.path.splitext(filename)[1].lower() in {".avi", ".wmv", ".rmvb", ".mkv", ".mp4", ".m4v"}:
			return True
		return 0
	def get_folders_with_no_video(self):
		fname = QtWidgets.QFileDialog.getExistingDirectory(self, "Folder to check")
		all_paths = {os.path.dirname(i["path"]) for i in core.find()}
		folders = {os.path.dirname(os.path.abspath(os.path.join(fname, filename))) if not os.path.isdir(os.path.join(fname, filename)) else os.path.abspath(os.path.join(fname, filename)) for filename in os.listdir(fname)}
		for folder in folders:
			if folder not in all_paths:
				self.debug(folder)
	
	def load_from_db(self):
		pass

	"""
	get_folder
	requests a dialog to select a folder
	identifies all video files within the folder (recursively) and loads the path into self.loaded with key 
	<code>
	Then, if entry is not in core, queries javlibrary for information on the JAV and loads it into core
	"""
	def get_folder(self):
		def get_code(dir):
			for i in os.listdir(dir):
				target = os.path.join(dir, i)
				temp = self.title_regex.search(target)
				if self.is_video(target):
					print("ISVIDEO: ", temp.groups())
					print(self.title_regex.findall(target))

				



				self.debug("i: " + i )
				if temp and self.is_video(target):
					#check if more than one file has the name

					if temp.groups() in {("SIS", "001"), ("ses", "23")}:
						print(temp)
						temp = self.title_regex.findall(os.path.splitext(target)[0])[-1]
						key = "-".join(temp).upper()

					else:
						key = "-".join(temp.groups()).upper()
					if key in self.loaded:
						self.loaded[key.upper()].append(os.path.abspath(i))
					else:
						self.debug("LOADING")
						self.debug(key)
						self.debug(os.path.abspath(target))
						self.loaded[key] = [os.path.abspath(target)]
					return
				else:
					if os.path.isdir(i):
						get_code(os.path.join(dir, i))

		fname = QtWidgets.QFileDialog.getExistingDirectory(self, "Load folder into JAVB", )
		self.statusBar().showMessage("Loading directory")
		if not fname:
			return
		total = len(os.listdir(fname))

		for num, i in enumerate(os.listdir(fname)):
			self.statusBar().showMessage("Loading {}/{}".format(num + 1, total))
			# create relative path
			target = os.path.join(fname, i)

			#if title detected in folder/file name
			temp = self.title_regex.search(target)
			self.debug("target: " + target)
			self.textwidget.text_edit.append(str(self.is_video(target)))
			self.textwidget.text_edit.append(str(os.path.isdir(os.path.join(fname, target))))
			if temp and self.is_video(target):
				key = "-".join(temp.groups())
				if key in self.loaded:
					self.loaded[key.upper()].append(os.path.abspath(target))
				else:
					self.loaded[key.upper()] = [os.path.abspath(target)]
			else:
				if os.path.isdir(target):
					self.debug("deep search" + target )
					get_code(target)
		# self.textwidget.text_edit.append(str(self.loaded))	
		# self.textwidget.text_edit.append("DONE")
		self.statusBar().showMessage("Acquiring information on Titles")
		count = len(self.loaded)
		for num, code in enumerate(self.loaded):
			self.statusBar().showMessage("Processing {}/{}:\t {}".format(num+1, count, code))
			if not core.find_one({"code": code}):
				#print("not code exists", "code")
				data = self.searcher.get_details(code)
				core.update_one({"code": code}, 
					{"$set":
					{
					"code": code.upper(),
					"title": data[0],
					"star": data[1].split(', '), 
					"tags": data[2].split(', '), 
					"maker": data[3], 
					"label": data[4],
					"path": self.loaded[code][0]
					}}, upsert = True)
			#print(code)
			data = core.find_one({"code": code})
			self.debug(code)
			self.debug("data" + str(data))
			try:
				self.loaded[code] = [data["title"], data["star"], data["tags"]]
			except:
				print(code)
				print(data)
				raise

		self.statusBar().showMessage("Loading complete")
	"""
	parse_row 
		takes a row, col (signal), acquires unique information and provides an entry
	"""
	def parse_row(self, row, col):
		table = self.textwidget.table_widget
		output = self.textwidget.display_widget
		indexes = table.selectionModel().selectedRows()
		code = table.item(row, 0).text()
		tags = table.item(row, 2).text()
		data = core.find_one({"code": code, "tags": tags.split(", ")})
		return data

	def select_row(self, row, col):
		data = self.parse_row(row, col)

	def open_row(self, row, col):
		data = self.parse_row(row, col)

	"""
	fix_missing:
	Scans the database for missing paths, removes all missing paths
	"""
	def fix_missing(self):
		total = core.count()
		for num, i in enumerate(core.find()):
			self.statusBar().showMessage("Checking {}/{}".format(num + 1, total))
			if not os.path.exists(i["path"]):
				core.remove({"_id": i["_id"]})

		
	def initUI(self):
		#Initialize menus

		exitAction = QtWidgets.QAction(QtGui.QIcon("exit.png"), "&Exit", self)
		exitAction.setShortcut("Ctrl+Q")
		exitAction.setStatusTip("Exit Application")
		exitAction.triggered.connect(QtWidgets.qApp.quit)

		openFold = QtWidgets.QAction(QtGui.QIcon("open.png"), "&Load folder", self)
		openFold.setShortcut("Ctrl+O")
		openFold.setStatusTip("Load folder into JAVB")
		openFold.triggered.connect(self.get_folder)

		save = QtWidgets.QAction(QtGui.QIcon("save.png"), "&Save edits", self)
		save.setShortcut("Ctrl+S")
		save.setStatusTip("Save edits made in table to database")
		save.triggered.connect(self.save)

		revert = QtWidgets.QAction(QtGui.QIcon("back.png"), "&Revert edits", self)
		revert.setShortcut("Ctrl+R")
		revert.setStatusTip("Revert edits made to to table.")

		# load = QtWidgets.QAction(QtGui.QIcon("open.png"), "&Load database", self)
		# load.setShortcut("Ctrl+L")
		# load.setStatusTip("Load folder into JAVB")
		# load.triggered.connect(self.load_database)

		fix_missing = QtWidgets.QAction(("&Fix missing files"), self)
		fix_missing.setStatusTip("Removes missing paths from database")
		fix_missing.triggered.connect(self.fix_missing)

		get_folders_with_no_video = QtWidgets.QAction(("&Find null folders"), self)
		get_folders_with_no_video.setStatusTip("Find folders containing no videos")
		get_folders_with_no_video.triggered.connect(self.get_folders_with_no_video)

		

		statusbar = self.statusBar()
		menubar = self.menuBar()
		# Menus of menubar
		fileMenu = menubar.addMenu("&File")
		fileMenu.addAction(exitAction)
		fileMenu.addAction(openFold)
		fileMenu.addAction(save)
		fileMenu.addAction(revert)

		editMenu = menubar.addMenu("&Edit")
		integrity = editMenu.addMenu("Integrity")
		integrity.addAction(fix_missing)
		integrity.addAction(get_folders_with_no_video)


		fileMenu = menubar.addMenu(editMenu)
		self.textwidget = TestWidget(self)
		self.setCentralWidget(self.textwidget)
		self.textwidget.control.push.clicked[bool].connect(lambda: self.textwidget.update_table())
		self.textwidget.table_widget.cellClicked.connect(self.select_row)
		self.textwidget.table_widget.cellDoubleClicked.connect(self.textwidget.open)
		self.textwidget.update_table()

		self.debug_widget = QtWidgets.QTextEdit()
		self.debug_widget.show()

		self.showMaximized()

class TestWidget(QWidget):
	def __init__(self, parent):
		self.parent = parent
		super().__init__()
		self.title_regex = re.compile(r"""([a-zA-Z]+)-?([0-9]+)""")

		self.text_edit = QtWidgets.QTextEdit()
		self.table_widget = QtWidgets.QTableWidget()
		self.table_widget.setRowCount(0)
		self.table_widget.setColumnCount(5)
		
		self.display_widget = DisplayWidget()
		self.display_widget.left.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

		self.control = ControlBox()

		self.test = DisplayWidget()
		self.control.search_field.textChanged.connect(self.search)

		self.init_UI()

		

	def init_UI(self):
		header_labels = ["Code", "Title", "Tags", "Actress", "_id"]
		self.table_widget.setHorizontalHeaderLabels(header_labels)
		self.table_widget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		self.table_widget.setColumnHidden(4, 1)


		grid = QtWidgets.QGridLayout()
		grid.setSpacing(10)

		grid.addWidget(self.text_edit, 0, 0)
		grid.addWidget(self.control, 1, 0)
		grid.addWidget(self.table_widget, 2, 0)
		grid.addWidget(self.display_widget, 6, 0)

		self.setLayout(grid)


	def debug(self, text):
		self.parent.debug(text)

	def update_table(self):
		data = [i for i in core.find().sort("code")]
		self.update_table_search(data)

	def open(self, row, col):
		target = ObjectId(self.table_widget.item(row, 4).text())
		
		path = core.find_one({"_id":target})["path"]

		if sys.platform.startswith("linux"):
			subprocess.call(["xdg-open", path])
		else:
			try:
				os.startfile(path)
			except:
				msg = QtWidgets.QMessageBox()
				msg.setIcon(QtWidgets.QMessageBox.Information)
				msg.setInformativeText("No Such file Exists")
				msg.setWindowTitle("Error")
				msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
				msg.exec_()




	def update_table_search(self, list_of_dicts):
		table = self.table_widget
		table.clearContents()
		table.setRowCount(0)
		for d in list_of_dicts:
			#print(d)
			row_count = table.rowCount()
			table.insertRow(row_count)
			table.setItem(row_count, 0, QtWidgets.QTableWidgetItem(d["code"]))
			table.setItem(row_count, 1, QtWidgets.QTableWidgetItem(d["title"]))
			table.setItem(row_count, 2, QtWidgets.QTableWidgetItem(", ".join(d["tags"])))
			table.setItem(row_count, 3, QtWidgets.QTableWidgetItem(", ".join(d["star"])))
			table.setItem(row_count, 4, QtWidgets.QTableWidgetItem(str(d["_id"])))
		return 0



	"""
	search
	Queries the database for the search term in the search box
	"""
	def search(self):
		search_widget = self.control.search_field
		text = search_widget.text().split()
		data = None
		if len(text) == 0:
			self.update_table()
			return 
		if self.control.dt2.isChecked():
			self.debug("TITLE")
			self.debug(text[0])
			data = core.find(
				{"code": {
					"$regex": text[0],
					"$options": "i"
				}}
				)
		
		# if searching for tags
		elif self.control.dt1.isChecked():
			# Title capitalization for tags
			text = [i.title().replace("_", " ") for i in text]
			self.debug("TAGS")
			data = core.find(
				{"tags": {"$all": text} })
		elif self.control.dt3.isChecked():
			text = [i.title().replace("_", " ") for i in text]
			self.debug("STARS")
			data = core.find(
				{"star": {"$all": text} })

		# If not title or tags, return nothing
		if not data:
			return

		data = sorted([i for i in data], key = lambda x: x["code"])

		self.update_table_search(data)



class DisplayWidget(QWidget):
	def __init__(self):
		super().__init__()
		self.init_UI()

	def init_UI(self):
		hbox = QtWidgets.QHBoxLayout(self)
		self.left = QtWidgets.QTextEdit(self)
		# right = QtWidgets.QLabel(self)
		# right.setScaledContents(True)
		# pixmap = QtGui.QPixmap("data\\images\\dummy.png")
		# right.setPixmap(pixmap)
		# splitter = QtWidgets.QSplitter()
		# splitter.addWidget(self.left)
		# splitter.addWidget(right)
		hbox.addWidget(self.left)
		# hbox.addWidget(right)
		hbox.setSpacing(10)
		hbox.setContentsMargins(0, 0, 0, 0)
		self.setLayout(hbox)

class ControlBox(QWidget):
	def __init__(self):
		super().__init__()

		self.init_UI()

	def init_UI(self):
		hbox = QtWidgets.QHBoxLayout()

		self.push = QtWidgets.QPushButton("Do")
		self.search_field = QtWidgets.QLineEdit()
		label = QtWidgets.QLabel("Search", self.search_field)
		hbox.addWidget(self.push	)
		hbox.addWidget(label)
		hbox.addWidget(self.search_field)

		self.dt1 = QtWidgets.QRadioButton("Tag")
		self.dt1.setChecked(True)
		self.dt2 = QtWidgets.QRadioButton("Code")
		self.dt3 = QtWidgets.QRadioButton("Star")
		hbox.addWidget(self.dt1)
		hbox.addWidget(self.dt2)
		hbox.addWidget(self.dt3)

		self.setLayout(hbox)


if __name__ == "__main__":
	app = QApplication(sys.argv)
	ex = Main()
	sys.exit(app.exec_())