import os
import re
import logging
import sqlite3

def is_video(self, file_path):
	file_name = os.path.split(file_path)[1]
	file_ext = os.path.splitext(file_name)[1]
	if file_ext.lower() in {".avi", ".wmv", ".rmvb", ".mkv", ".mp4", ".m4v"}:
		return True
	return False

def get_folders_with_no_video(dir_path):
	pass




class Manager:
	def __init__(self):
		self.title_regex = re.compile(r"""([a-zA-Z]+)-?([0-9]+)""")

	def collect_videos_in_dir(self, dir_path, debug = 0):
		"""
		Collects all paths to all videos in target directory
		returns a list of (JAV_code, video_abspath) tuples
		"""
		def get_code(dir):
			#for each file/dir in folder
			for i in os.listdir(dir):
				target = os.path.join(dir, i)
				if os.path.isfile(target) and is_video(target):





				
				temp = self.title_regex.search(target)
				if is_video(target):
					print("ISVIDEO: ", temp.groups())
					print(self.title_regex.findall(target))
				self.debug("i: " + i )
				if temp and is_video(target):
					# check if more than one file has the name
					# Filter out known fake names
					if temp.groups() in {("SIS", "001"), ("ses", "23")}:
						print(temp)
						temp = self.title_regex.findall(os.path.splitext(target)[0])[-1]
						key = "-".join(temp).upper()

					else:
						key = "-".join(temp.groups()).upper()
					if key in self.loaded:
						self.loaded[key.upper()].append(os.path.abspath(i))
					else:
						pass
						# self.debug("LOADING")
						# self.debug(key)
						# self.debug(os.path.abspath(target))
						# self.loaded[key] = [os.path.abspath(target)]
					return
				else:
					if os.path.isdir(i):
						get_code(os.path.join(dir, i))