import os
import re
import requests
from libs.tagpy import TagEngine, Col, Table, TagTable

class Searcher:
	"""
	Core searcher class
	"""
	def __init__(self):
		self.site = 'http://www.javlibrary.com/en/vl_searchbyid.php?keyword='
		self.star_re = re.compile(r'vl\_star\.php\?s\=[a-z0-9]{1,10}" rel="tag">([a-zA-Z\s]+)<')
		self.director_re = re.compile(r'vl\_director\.php\?d=[a-z0-9]+" rel="tag">(.{1,20})</a>')
		self.genre_re = re.compile(r'vl\_genre\.php\?g=[a-z0-9]+" rel="category tag">([a-zA-Z1234\s]+)')
		self.maker_re = re.compile(r'vl\_maker\.php\?m=[a-z0-9]+"\srel="tag">([a-zA-Z.\s()-]+)')
		self.label_re = re.compile(r'vl\_label\.php\?l=[a-z0-9]+"\srel="tag">([a-zA-Z.\s()-]+)')
		self.title_re = re.compile(r'<meta property="og:title" content=".*? (.*) - JAVLibrary" \/>')

		self.jp_site = 'http://www.javlibrary.com/ja/vl_searchbyid.php?keyword='
		self.jp_star_re = re.compile(r'vl\_star\.php\?s\=[a-z0-9]{1,10}" rel="tag">(.{1,20})</a>')
		self.jp_genre_re = re.compile(r'vl\_genre\.php\?g=[a-z0-9]+" rel="category tag">(.{1,20})</a>')
		self.jp_director_re = re.compile(r'vl\_director\.php\?d=[a-z0-9]+" rel="tag">(.{1,20})</a>')
		self.jp_maker_re = re.compile(r'vl\_maker\.php\?m=[a-z0-9]+"\srel="tag">(.{1,20})</a>')
		self.jp_label_re = re.compile(r'vl\_label\.php\?l=[a-z0-9]+"\srel="tag">(.{1,20})</a>')
		self.jp_title_re = re.compile(r'<meta property="og:title" content=".*? (.*) - JAVLibrary" \/>')

	def get_details(self, code, get_image = 0):
		page = requests.get(self.site + code)
		text = page.text
		title = self.title_re.findall(text)[0]
		star = self.star_re.findall(text)
		genre = self.genre_re.findall(text)
		maker = self.maker_re.findall(text)[0]
		label = self.label_re.findall(text)[0]
		director = self.director_re.search(text)
		if not director:
			director = ""
		else:
			director = director.groups(0)[0]

		res = {
		"code": code,
		"title": title,
		"stars": star, 
		"tags": genre,
		"maker": maker, 
		"label": label,
		"director": director
		}
		return res

	def get_details_jp(self, code, get_image = 0):
		page = requests.get(self.jp_site + code)
		text = page.text
		title = self.jp_title_re.findall(text)[0]
		star = self.jp_star_re.findall(text)
		genre = self.jp_genre_re.findall(text)
		maker = self.jp_maker_re.findall(text)[0]
		label = self.jp_label_re.findall(text)[0]
		director = self.jp_director_re.search(text)
		if not director:
			director = ""
		else:
			director = director.groups(0)[0]
		res = {
		"code": code,
		"title": title,
		"stars": star, 
		"tags": genre,
		"maker": maker, 
		"label": label,
		"director": director
		}
		return res

class Manager:
	def __init__(self):
		self.init_tag_engine()
		self.searcher = Searcher()

	def init_tag_engine(self):
		item_table = Table("videos")
		code = Col("code", str, nocase = True)
		title = Col("title", str, nocase = True)
		maker = Col("maker", str, nocase = True)
		director = Col("director", str, nocase = True)
		label = Col("label", str, nocase = True)
		path = Col("path", str, unique = True)

		code_jp = Col("code_jp", str, nocase = True)
		title_jp = Col("title_jp", str, nocase = True)
		maker_jp = Col("maker_jp", str, nocase = True)
		director_jp = Col("director_jp", str, nocase = True)
		label_jp = Col("label_jp", str, nocase = True)
		item_table.add_cols(
			code, title, maker, director, label,
			code_jp, title_jp, maker_jp, director_jp, label_jp, 
			path
			)

		tag_table = TagTable("tags")
		tag_name = Col("tag_name", str, unique = True, nocase = True)
		tag_name_jp = Col("tag_name_jp", str, unique = True, nocase = True)
		tag_type = Col("tag_type", str, nocase = True)
		tag_table.add_cols(tag_name, tag_type, tag_name_jp)
		self.te = TagEngine(os.path.join("data","db.sqlite3"), item_table, tag_table)
		self.te.setup_tables()
	
	def tag_videos(self, dir_path, debug = 0 ):
		videos = get_video_paths(dir_path)
		linked, unlinked = link_code_to_videos(videos)
		pass
		#IMPLEMENT THIS

	def tag_linked_videos(self, linked_videos, debug =0):
		tag_tag_ids = {i[1]:i[0] for i in self.te.get_all_tags()}

		for code, path in linked_videos:
			failures = []
			try:
				data = self.searcher.get_details(code)
				data_jp = self.searcher.get_details_jp(code)
			except:
				failures.append(code)
			if debug:
				print("adding item {} with path {}".format(code, path))

			# catch null results from searcher
			if data["code"] != code:
				failures.append((code, path))
				continue
			item = self.te.add_item(data["code"], data["title"], data["maker"], data["director"], data["label"],
							 data_jp["code"], data_jp["title"], data_jp["maker"], data_jp["director"], data_jp["label"],
							 path, debug = 1)
			item_id = item[0]

			# add tags if they don't exist else get the tag_id and tag the data
			for tag_name, jp_tag_name in zip(data["tags"], data_jp["tags"]):
				if tag_name in tag_tag_ids:
					tag_id = self.te.get_tag(tag_name)[0]
				else:
					tag= self.te.add_tag(tag_name, tag_name, "content", jp_tag_name)
					tag_id = tag[0]
					tag_tag_ids[tag_name] = tag_id
				self.te.tag_item(item_id, tag_id)

			for tag_name, jp_tag_name in zip(data["stars"], data_jp["stars"]):
				if tag_name in tag_tag_ids:
					tag_id = self.te.get_tag(tag_name)[0]
				else:
					tag= self.te.add_tag(tag_name, tag_name, "star", jp_tag_name)
					tag_id = tag[0]
					tag_tag_ids[tag_name] = tag_id
				print("tagging item : ", item_id, tag_id)	
				self.te.tag_item(item_id, tag_id)

		return failures

	def search(self, string, and_flag = False):
		"""
		Searches the database for entries matching a specific tag. String is processed
		and tag-type data acquired and used to search
		"""
		terms = string.lower().split()
		terms = [i.replace("_", " ").split(":") for i in terms]
		tag_data = [i[::-1] if len(i) != 1 else (i[0], "") for i in terms ]
		print("TAG_DATA", tag_data)
		return self.te.search_tags_field(tag_data , "tag_type", and_flag = and_flag)

def is_video(file_path):
	file_name = os.path.split(file_path)[1]
	file_ext = os.path.splitext(file_name)[1]

	if file_ext.lower() in {".avi", ".wmv", ".rmvb", ".mkv", ".mp4", ".m4v", ".webm", ".flv", ".vob", ".mov",
							".m4p", ".mpg", ".mp2", ".mpeg"}:
		return True
	return False

def get_dirs_without_video(dir_path):
	# purge trailing slashes
	dir_path = dir_path.rstrip("\\")
	dir_path = dir_path.rstrip("/")
	res = []
	for path, dirs, files in os.walk(dir_path):
		if os.path.split(path)[0] != dir_path:
			# print(os.path.split(path)[0], dir_path)
			continue
		if not any(is_video(file) for file in files):
			res.append(path)
	return res


def get_video_paths(dir_path):
	# purge trailing slashes
	dir_path = dir_path.rstrip("\\")
	dir_path = dir_path.rstrip("/")
	res = []
	for path, dirs, files in os.walk(dir_path):
		for file in files:
			if is_video(file):
				res.append(os.path.join(path, file))
	return res

def link_code_to_videos(video_paths):
	"""
	links video codes for all videos provided by get_video_paths
	input: list of strings. Each string is a path to a video file
	returns: tuple containing two lists (linked, unlinked). Linked includes files which have
			 been successfully linked, unlinked for files with no successful link

	"""
	title_regex = re.compile(r"""([a-zA-Z]+)-?([0-9]+)""")
	linked = []
	unlinked = []
	false_flags = {("SIS", "001"), ("ses", "23")}
	for path in video_paths:
		candidate = title_regex.search(path).groups()
		#print(candidate)
		# filter out known false flags
		if candidate in false_flags:
			candidate = title_regex.findall(path)[-1]
		if not candidate:
			unlinked.append(path)
			continue
		key = "-".join(candidate).upper()
		linked.append((key, path))
	return (linked, unlinked)

if __name__ == "__main__":
	video_paths = get_video_paths("data\\test_data")
	# # print(video_paths)
	# # print(len(video_paths))
	# no_videos = get_dirs_without_video("data\\test_data\\")
	# # print(no_videos)
	# # print(len(no_videos))
	linked_videos, unlinked = link_code_to_videos(video_paths)
	# print(linked_videos)

	test_data = linked_videos[:5]
	print(test_data)

	m = Manager()
	
	# m.te.reset_tables()
	# m.__init__()
	# m.tag_linked_videos(test_data, debug = 1)

	# print(m.search("star:atomi_shuri 3p", and_flag = True))
	print(m.search("solowork uniform"))

	# s = Searcher()
	# print(s.get_details_jp("HODV-21196"))
	# print(s.get_details("HODV-21196"))
	# print(s.get_details("MUM-129"))