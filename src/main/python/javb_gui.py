#!/usr/bin/env python3

import sys

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import Qt

import javb_core as core

class Main(QMainWindow):
	def __init__(self):
		super().__init__()
		self.initUI()
		self.manager = core.Manager()
		self.loaded = dict()

	def init_menus(self):

		statusbar = self.statusBar()
		menubar = self.menuBar()
		# Load folder
		load_folder = QtWidgets.QAction(QtGui.QIcon("open.png"), "&Load folder", self)
		load_folder.setShortcut("Ctrl+L")
		load_folder.setStatusTip("Load target directory into JAVB")

		save_action = QtWidgets.QAction(QtGui.QIcon("save.png"), "&Save edits", self)
		save_action.setShortcut("Ctrl+S")
		save_action.setStatusTip("Save edits made in table to database")
		# save.triggered.connect(self.save)

		exit_action = QtWidgets.QAction(QtGui.QIcon("exit.png"), "&Quit JAVBrowse", self)
		exit_action.setShortcut("Ctrl+Q")
		exit_action.setStatusTip("Quit JAVBrowse")

		file_menu = menubar.addMenu("&File")
		file_menu.addAction(load_folder)
		file_menu.addAction(save_action)
		file_menu.addAction(exit_action)


	def initUI(self):
		self.init_menus()
		self.initTable()
		self.setCentralWidget(self.table_display)

		self.debug_widget = QtWidgets.QTextEdit()
		self.debug_widget.show()

		self.showMaximized()

	def initTable(self):
		self.table_display = TableDisplay(self)
		self.table_display.control_widget.search_field.textChanged.connect(self.search)

	def search(self):
		"""
		Takes input from control box of table widget and searches the tag_database
		of the core system
		"""
		string = self.table_display.get_search_text()
		entries = self.manager.search(string, and_flag = self.table_display.get_search_status())
		self.table_display.update_table(entries)

	def debug(self, *args):
		temp = " ".join([str(i) for i in args])
		self.debug_widget.append(temp)




class ControlBox(QWidget):
	def __init__(self):
		super().__init__()

		self.init_UI()

	def init_UI(self):
		hbox = QtWidgets.QHBoxLayout()

		self.push = QtWidgets.QPushButton("Do")
		self.search_field = QtWidgets.QLineEdit()
		label = QtWidgets.QLabel("Search", self.search_field)
		hbox.addWidget(self.push	)
		hbox.addWidget(label)
		hbox.addWidget(self.search_field)

		self.dt1 = QtWidgets.QRadioButton("OR")
		self.dt1.setChecked(True)
		self.dt2 = QtWidgets.QRadioButton("AND")
		hbox.addWidget(self.dt1)
		hbox.addWidget(self.dt2)

		self.setLayout(hbox)

class TableDisplay(QWidget):
	def __init__(self, parent):
		super().__init__()
		self.parent = parent
		self.init_UI()
	def init_UI(self):
		self.control_widget = ControlBox()
		self.init_table()
		self.init_layout()

	def init_table(self):
		header_labels = [
		"_id", 
		# English
		"Code", 
		"Title", 
		"Maker",
		"Director",
		"Label",
		# JAPANESE
		"Code", 
		"Title", 
		"Maker",
		"Director",
		"Label",
		"_path"]
		self.table_widget = QtWidgets.QTableWidget()
		self.table_widget.setRowCount(0)
		self.table_widget.setColumnCount(20)
		self.table_widget.setHorizontalHeaderLabels(header_labels)
		self.table_widget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		# self.table_widget.setColumnHidden(4, 1)

	def init_layout(self):
		grid = QtWidgets.QGridLayout()
		grid.setSpacing(10)
		grid.addWidget(self.control_widget, 1, 0)
		grid.addWidget(self.table_widget, 2, 0)
		self.setLayout(grid)

	# Controlbox management

	def get_search_status(self):
		if self.control_widget.dt1.isChecked():
			return "OR"
		else:
			return "AND"

	def get_search_text(self):
		return self.control_widget.search_field.text()

	def update_table(self, entries):
		self.table_widget.clearContents()
		self.table_widget.setRowCount(0)
		# Populate rows
		for row, entry in enumerate(entries):
			self.table_widget.insertRow(row)
			print(entry)
			for num, datum in enumerate(entry):
				self.table_widget.setItem(row, num, QtWidgets.QTableWidgetItem(str(datum)))
		
		



if __name__ == "__main__":
	app = QApplication(sys.argv)
	ex = Main()
	sys.exit(app.exec_())
